# Test mobile apps with Appium #
http://appium.io/
```
$ sudo npm install -g appium appium-doctor nightwatch wd --unsafe-perm=true
$ sudo npm audit fix --force --unsafe-perm=true
```

some references:
*  https://www.genbetadev.com/herramientas/automatizacion-de-pruebas-web-moviles-appium-nightwatch-js

*  http://www.shaneofalltrades.com/2016/08/26/Mobile-Web-Testing-Using-Nightwatchjs-part-1/
*  http://www.shaneofalltrades.com/2016/08/26/Mobile-Web-Testing-Using-Nightwatchjs-part-2/

## Android SDK ##
1. (not working, go directly to point 2.)

  *  Download sdk-tools-linux-4333796.zip from https://developer.android.com/studio/#downloads
  *  Extract to ~/android-sdk-linux/tools
```
$ nano ~/.bashrc
  export ANDROID_HOME=~/android-sdk-linux/tools
```
  *  Error:
    [MJSONWP] Encountered internal error running command: Error: Could not find adb in ...
              ... Do you have the Android SDK installed at '/home/dev10/android-sdk-linux/tools'?
    => not working, go for 2.
    ```
    $ rm -rf ~/android-sdk-linux
    ```

2.

  *  Download Android Studio from https://developer.android.com/studio/
  *  Extract to ~/android-studio
  *  Follow instructions in ~/android-studio/Install-Linux-tar.txt
  *  After installation, a new directory ~/android-sdk-linux is created
```
$ nano ~/.bash_profile
  export PATH=$PATH:~/android-sdk-linux/tools/tools/
  export PATH=$PATH:~/android-sdk-linux/tools/platform-tools/
  export ANDROID_HOME=~/android-sdk-linux/tools
  export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.171-8.b10.el7_5.x86_64/jre
  export PATH=$JAVA_HOME/bin:$PATH

$ appium-doctor
```

*  create a new app in Android Studio
*  open AVD Manager
*  create a virtual device

### Native app ###
Download a native app and store it in **apps/**, for example:
```
apps/201magazine_android_2812_release_android_2.8.43.stable_20180606072518.apk
```
and point the file on **config/nightwatch.json** in **test_settings/app1/desiredCapabilities/app**

## How to run ##
*  run virtual device from AVD Manager
```
  $ /home/dev10/android-studio/bin/studio.sh
```
*  open a terminal
```
  $ appium
```
*  open another terminal
```
  $ nightwatch -c config/nightwatch.json -e android
  $ nightwatch -c config/nightwatch.json -e android --test src/tests/example1.js --testcase "Tecnavia"
  $ nightwatch -c config/nightwatch.json -e app1 --test src/tests/example1.js --testcase "App"
```
