module.exports = {
 // "Demo test Google" : function (browser) {
 //   browser
 //     .url("http://www.google.com")
 //     .waitForElementVisible('body', 1000)
 //     .setValue('input[type=search]', 'genbetadev.com')
 //     .waitForElementVisible('button[name=btnG]', 1000)
 //     .click('button[name=btnG]')
 //     .pause(1000)
 //     .assert.containsText('#main', 'genbeta')
 //     .end();
 //   },

  "Tecnavia" : function (browser) {
    browser
      .url("http://www.tecnavia.com")
      .assert.containsText('.container .avia_textblock', 'E-PUBLISHING SOLUTIONS THAT MEET AND EXCEED YOUR EXPECTATIONS')
      .end();
    },

  "App": function (client) {
    client
      .init()
      .pause(20000)
      .end();
  }
};
